package com.company;

public enum Cartas {
    AS(1),
    DOIS(2),
    TRÊS(3),
    QUARTRO(4),
    CINCO(5),
    SEIS(6),
    SETE(7),
    OITO(8),
    NOVE(9),
    DEZ(10),
    DAMA(10),
    VALETE(10),
    REIS(10);

    public int valor;

    public int getValor() {
        return valor;
    }
    Cartas(int valor){
        this.valor = valor;
    }




}
