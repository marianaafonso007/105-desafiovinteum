package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Partida extends Jogo {
    protected List<Cartas> mesa;

    public Partida() {
        this.mesa = new ArrayList<>();
    }

    public void sortear() {
        boolean cartaRepetida = false;
        Random random = new Random();
        do{
            Cartas carta = Cartas.values()[random.nextInt(Cartas.values().length)];
            if (mesa.contains(carta)){
                cartaRepetida = true;
            }else{
                this.mesa.add(carta);
                cartaRepetida = false;
            }
        }while (cartaRepetida);
        pontuacaoPartida();
        System.out.println(mesa);
    }

    public boolean analisarPartidaRegra()
    {
        if(this.valorPartida <= 20){
            return true;
        }else {
            return false;
        }
    }

    public void pontuacaoPartida()
    {
        this.valorPartida = 0;
        for (Cartas carta: this.mesa){
            this.valorPartida += carta.getValor();
        }
    }

}
