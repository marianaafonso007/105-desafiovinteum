package com.company;

import java.util.Scanner;

public class UI {
    Partida partida = new Partida();
    Scanner entrada = new Scanner(System.in);
    Historico hist = new Historico();

    public void starJogo(){
        boolean statusJogo = false;
        int valor = 0;
        System.out.println("Olá, vamos jogar o jogo Vinte e Um! Para iniciar digite 1 ou para sair digite 2");
        valor = entrada.nextInt();
        if(valor == 1){
            do {
                partida.sortear();
                statusJogo = partida.analisarPartidaRegra();
                if (!statusJogo && partida.getValorPartida() == 21){
                    hist.addHistoricoJogo(partida);
                    mensagemUser("Parabéns você ganhou: "+ partida.getValorPartida());
                    mensagemUser("Deseja continuar? Digite 1 para sim ou 2 para sair:");
                    valor = entrada.nextInt();
                    if (valor == 1) {
                        partida = new Partida();
                    }
                }else if (statusJogo){
                    mensagemUser("Valor Partida: "+ partida.getValorPartida());
                    mensagemUser("Deseja pegar outra carta (digite 1) ou prefere desistir (digite 2)?");
                    valor = entrada.nextInt();
                    if (valor != 1) {
                        hist.addHistoricoJogo(partida);
                        mensagemUser("Partida encerada: "+ partida.getValorPartida());
                        partida = new Partida();
                        valor = 1;
                    }
                }else {
                    mensagemUser("Partida encerada: "+ partida.getValorPartida());
                    mensagemUser("Perdeu Deseja iniciar uma nova partida (digite 1) ou prefere sair (digite 2)?");
                    valor = entrada.nextInt();
                    if (valor == 1) {
                        partida = new Partida();
                    }

                }
            }while (valor == 1);
        }
//        hist.listarHistorico();
        hist.validarMelhorPartida();
    }

    public static void mensagemUser(String mensagem){
        System.out.println(mensagem);
    }

}
