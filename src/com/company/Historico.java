package com.company;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Historico {
    protected List<Partida> historicoJogo = new ArrayList<>();
    private int valorMaiorPartida = 0;
    private int indiceMelhorPartida;

    public List<Partida> getHistoricoJogo() {
        return historicoJogo;
    }

    public void setHistoricoJogo(List<Partida> historicoJogo) {
        this.historicoJogo = historicoJogo;
    }

    public void addHistoricoJogo(Partida partida){
        this.historicoJogo.add(partida);
    }

    public void listarHistorico(){
        for(int i = 0; i < historicoJogo.size(); i++){
            System.out.println("Partida número "+ (i+1) + ": " + historicoJogo.get(i).mesa.toString()
                    + " Valor Partida: " + historicoJogo.get(i).getValorPartida());
        }
    }

    private void maiorValorPartida(int valor, int indice){
        this.valorMaiorPartida = valor;
        this.indiceMelhorPartida = indice;
    }

    public void validarMelhorPartida(){
        for(int i = 0; i < historicoJogo.size(); i++){
           if (historicoJogo.get(i).valorPartida > valorMaiorPartida){
               maiorValorPartida(historicoJogo.get(i).valorPartida, i);
           }
        }

        System.out.println("Sua melhor partida foi: " + historicoJogo.get(this.indiceMelhorPartida).mesa + ". Onde acumulou no total: " +  this.valorMaiorPartida);
    }

}
